/*
Made by Isoware
Last update: 2018.07.02
*/

window.addEventListener('load', function() {
    /*
    ** Messages
    */
    let afficherMessage = function(message, erreur=true) {
        let balise = document.querySelector("#message");
        balise.textContent = message;
        if (!erreur) balise.style.color = 'green';
        balise.style.display = 'block';
    }

    let supprimerMessage = function() {
        document.querySelector("#message").style.display = 'none';
        document.querySelector('#message').style.color = 'rgb(150, 40, 27)';
    }

    /*
    **  Partie Design
    */
    let changerBoutton = function(bouton) {
        if(bouton == "connexion") {
            document.querySelector("#btn_connexion").style.backgroundColor = "rgb(151, 160, 175)";
            document.querySelector("#btn_inscription").style.backgroundColor = "rgb(236,236,236)";
            document.querySelector("#fade").style.opacity = 0;
            supprimerMessage();
            document.querySelector("#fenetre").style.height = '290px';
            setTimeout(function() {
                document.querySelector("#inscription").style.display = "none";
                document.querySelector("#connexion").style.display = "block";
                document.querySelector("#fade").style.opacity = 1;
            }, 400);
        } else {
            document.querySelector("#btn_inscription").style.backgroundColor = "rgb(151, 160, 175)";
            document.querySelector("#btn_connexion").style.backgroundColor = "rgb(236,236,236)";
            document.querySelector("#fade").style.opacity = 0;
            supprimerMessage();
            document.querySelector("#fenetre").style.height = '432px';
            setTimeout(function() {
                document.querySelector("#connexion").style.display = "none";
                document.querySelector("#inscription").style.display = "block";
                document.querySelector("#fade").style.opacity = 1;
            }, 400);
        }
    }

    /*
    **  Partie verification
    */
   var verifPseudo = function() {
       let pseudo = document.querySelector('#pseudo');
       if (pseudo.value === '') {
           pseudo.style.backgroundColor = "#FE5353";
           afficherMessage('Le pseudo est vide');
       } else if (pseudo.value.length < 5) {
           pseudo.style.backgroundColor = "#FE5353";
           afficherMessage('Pseudo d\'au moins 5 caractères');
       } else {
            pseudo.style.backgroundColor = 'white';
            supprimerMessage();
       }
   }
    var verifEmail = function() {
        let reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
        if(!reg.test(document.querySelector("#adresse").value)) {
            document.querySelector("#adresse").style.backgroundColor = "#FE5353";
            afficherMessage('Votre adresse mail n\'est pas valide.');
        } else {
            document.querySelector("#adresse").style.backgroundColor = "white";
            supprimerMessage();
        }
    };
    var verifMdp = function() {
        //Verifie si il y a au moins une majuscule, une minuscule, un chiffre et entre 6 et 15 caractères
        let reg = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])^.{6,15}$'); 

        if (!reg.test(document.querySelector("#mdp").value)) {
            document.querySelector("#mdp").style.backgroundColor = "#FE5353";
            afficherMessage('Le mot de passe doit contenir au moins une majuscule, une minuscule et un chiffre.');

        } else {
            document.querySelector("#mdp").style.backgroundColor = "white";
            supprimerMessage();
        }
    };
    var verifConfMdp = function() {
        if(document.querySelector("#confMdp").value != document.querySelector("#mdp").value) {
            document.querySelector("#confMdp").style.backgroundColor = "#FE5353";
            afficherMessage('Les mots de passes ne correspondent pas.');
        }
        else {
            document.querySelector("#confMdp").style.backgroundColor = "white";
            supprimerMessage();
        }
    };
    // Trigger des boutons
    document.querySelector("#btn_connexion").addEventListener('click', function() { changerBoutton('connexion'); });
    document.querySelector("#btn_inscription").addEventListener('click', function() { changerBoutton('inscription'); });

    //Verification pseudo
    document.querySelector('#pseudo').addEventListener('keydown', verifPseudo);
    document.querySelector('#pseudo').addEventListener('keyup', verifPseudo);

    //Verification email
    document.querySelector("#adresse").addEventListener('keydown', verifEmail);
    document.querySelector("#adresse").addEventListener('keyup', verifEmail);

    //Verification fiabilité mot de passe
    document.querySelector("#mdp").addEventListener('keyup', verifMdp);
    document.querySelector("#mdp").addEventListener('keydown', verifMdp);

    //Verification concordance des mots de passe
    document.querySelector("#confMdp").addEventListener('blur', verifConfMdp);
    document.querySelector("#confMdp").addEventListener('keydown', verifConfMdp);
    document.querySelector("#confMdp").addEventListener('keyup', verifConfMdp);
    document.querySelector("#confMdp").addEventListener('focus', verifConfMdp);
});
